import { Component, OnInit } from '@angular/core';
import { PollServiceService } from '../poll-service.service';
import { FormControl, FormGroup,FormBuilder, Validators} from '@angular/forms';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  profileForm: FormGroup;
  

  constructor(
  //private pollService:PollServiceService
  private adminfb: FormBuilder) { 
    this.createForm();
  }

  createForm(){
    this.profileForm = this.adminfb.group({
      userId:   ['', [Validators.required,Validators.minLength(4),Validators.maxLength(12)]],
      email:    ['', [Validators.required,Validators.email]],
      password: ['', [Validators.required,Validators.minLength(4), Validators.maxLength(8)]]
      })

  }
  get userid(){
    return this.profileForm.controls.userId;
  }
  get uesrEmail(){
    return this.profileForm.controls.email;
  }
  get userPassword(){
    return this.profileForm.controls.password;
  }

  ngOnInit() {
    
  }

}
