import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup,FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  

  constructor(private loginfb: FormBuilder) {
    this.createForm();
   }

  createForm(){
    this.loginForm = this.loginfb.group({
      loginuserId:   ['', [Validators.required,Validators.minLength(4),Validators.maxLength(12)]],
      loginemail:    ['', [Validators.required,Validators.email]],
      loginpassword: ['', [Validators.required,Validators.minLength(4),Validators.maxLength(8)]]
      })
    }
      get loginuserid(){
        return this.loginForm.controls.loginuserId;
      }
      get loginuesrEmail(){
        return this.loginForm.controls.loginemail;
      }
      get loginuserPassword(){
        return this.loginForm.controls.loginpassword;
      }

  ngOnInit() {
  }

}
